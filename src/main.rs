use std::io::stdin;
use std::env;
use std::io::Write;
use std::path::Path;
use std::process::Command;
use std::process::Stdio;

fn update() {
    let path = Path::new("/home/robin/dev/nutshell/");
    env::set_current_dir(&path).unwrap();
    Command::new("cargo")
        .arg("run")
        .arg("--release")
        .arg("copy")
        .stdout(Stdio::inherit())
        .stdin(Stdio::inherit())
        .stderr(Stdio::inherit())
        .output()
        .unwrap();
}

fn copy() {
}

fn main() {
    match env::args().skip(1).next() {
        Some(v) => match v.as_str() {
            "update" => {
                update();
                },
            "copy" => {
                Command::new("sudo")
                    .arg("cp")
                    .arg("target/release/nutshell")
                    .arg("/usr/bin")
                    .arg("-f")
                    .stdin(Stdio::inherit())
                    .stdout(Stdio::inherit())
                    .stderr(Stdio::inherit())
                    .output()
                    .unwrap();
                std::thread::sleep(std::time::Duration::from_secs(1));
                },

                _ => {},
        },
        None => {},
    }

    loop {
        let mut foo = "".to_owned();
        print!("{} > ", env::current_dir().unwrap().display());
        std::io::stdout().flush().unwrap();
        stdin().read_line(&mut foo).unwrap();
        let mut foo = foo.lines().next().unwrap().split(' ');
        let i = foo.next().unwrap();
        match i {
            "cd" => {
                let u = match foo.next() {
                    Some(v) => v,
                    None => {
                        let home_dir = env::var("HOME").unwrap();
                        let path = Path::new(&home_dir);
                        env::set_current_dir(&path).unwrap();
                        continue
                    },
                };
                if u == "" {
                    let home_dir = env::var("HOME").unwrap();
                    let path = Path::new(&home_dir);
                    env::set_current_dir(&path).unwrap();
                    continue
                }
                let path = Path::new(u);
                env::set_current_dir(&path).unwrap();
            },
            "update" => {
                update();
                Command::new("sudo")
                    .arg("cp")
                    .arg("target/release/rsh")
                    .arg("/usr/bin")
                    .arg("-f")
                    .stdin(Stdio::inherit())
                    .stdout(Stdio::inherit())
                    .stderr(Stdio::inherit())
                    .output()
                    .unwrap();
                },
            "exit" => {
                break;
            },
            _ => {
                match Command::new(i)
                    .args(foo)
                    .stdout(Stdio::inherit())
                    .stdin(Stdio::inherit())
                    .stderr(Stdio::inherit())
                    .output() {
                        Ok(_) => (),
                        Err(_) => {
                            println!("Command not found!");
                            continue;
                        }
                    }
            },
        }
    }
}
